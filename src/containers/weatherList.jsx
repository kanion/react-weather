import _ from 'lodash';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import Chart from '../components/chart';

class WeatherList extends Component {
    renderWeather(cityData) {
        let id = cityData.city.id,
            name = cityData.city.name,
            temps = _.map(cityData.list.map(weather => weather.main.temp), (temp) => temp -273),
            pressures = cityData.list.map(weather => weather.main.pressure),
            humidities = cityData.list.map(weather => weather.main.humidity);

        return (
            <tr key={id}>
                <td>{name}</td>
                <td>
                    <Chart data={temps} color="orange"/>
                </td>
                <td>
                    <Chart data={pressures} color="green"/>
                </td>
                <td>
                    <Chart data={humidities} color="black"/>
                </td>
            </tr>
        )
    }

    render() {
        console.log(this.props);
        return (
            <table className="table table-hover">
                <thead>
                <tr>
                    <th>City</th>
                    <th width="20%">Temp</th>
                    <th width="20%">Pressure</th>
                    <th width="20%">Humidity</th>
                </tr>
                </thead>
                <tbody>
                {this.props.weather.weather.map(this.renderWeather)}
                </tbody>
            </table>
        )
    }
}

function mapStateToProps(weather) {
    return {weather}
}

export default connect(mapStateToProps)(WeatherList);