import axios from 'axios';

// moj klucz
const API_KEY = '326f0c3327b9726a9243fe529d262f75';
// const API_KEY = 'b1b15e88fa797225412429c1c50c122a1';

const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;
// const ROOT_URL = `http://openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city) {
	const url = `${ROOT_URL}&q=${city},us`;
	const request = axios.get(url);

	return {
		type: FETCH_WEATHER,
		payload: request
	}
}